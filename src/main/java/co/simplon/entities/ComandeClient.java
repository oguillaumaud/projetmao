package co.simplon.entities;

import java.util.List;

public class ComandeClient {

private int prixTotal;
private int numéroTable;
private List <Plat> Listplat;
private List <Menu> listmenu;

public int getPrixTotal() {
    return prixTotal;
}



public void setPrixTotal(int prixTotal) {
    this.prixTotal = prixTotal;
}



public int getNuméroTable() {
    return numéroTable;
}



public void setNuméroTable(int numéroTable) {
    this.numéroTable = numéroTable;
}



public List<Plat> getListplat() {
    return Listplat;
}



public void setListplat(List<Plat> listplat) {
    Listplat = listplat;
}



public List<Menu> getListmenu() {
    return listmenu;
}



public void setListmenu(List<Menu> listmenu) {
    this.listmenu = listmenu;
}



public ComandeClient(int prixTotal) {
    this.prixTotal = prixTotal;
}



public ComandeClient(int prixTotal, int numéroTable) {
    this.prixTotal = prixTotal;
    this.numéroTable = numéroTable;
}



public ComandeClient(int prixTotal, int numéroTable, List<Plat> listplat) {
    this.prixTotal = prixTotal;
    this.numéroTable = numéroTable;
    Listplat = listplat;
}



public ComandeClient(int prixTotal, int numéroTable, List<Plat> listplat, List<Menu> listmenu) {
    this.prixTotal = prixTotal;
    this.numéroTable = numéroTable;
    Listplat = listplat;
    this.listmenu = listmenu;
}


}
