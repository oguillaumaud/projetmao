package co.simplon.entities;

import java.sql.Date;
import java.util.List;

public class Commande {
private List<Ingredient> lisngrtingredients;
private int prixTotal;
private Date date;

public Commande() {
}

public Commande(Date date) {
    this.date = date;
}

public Commande(int prixTotal, Date date) {
    this.prixTotal = prixTotal;
    this.date = date;
}

public Commande(List<Ingredient> lisngrtingredients, int prixTotal, Date date) {
    this.lisngrtingredients = lisngrtingredients;
    this.prixTotal = prixTotal;
    this.date = date;
}

public List<Ingredient> getLisngrtingredients() {
    return lisngrtingredients;
}

public void setLisngrtingredients(List<Ingredient> lisngrtingredients) {
    this.lisngrtingredients = lisngrtingredients;
}

public int getPrixTotal() {
    return prixTotal;
}

public void setPrixTotal(int prixTotal) {
    this.prixTotal = prixTotal;
}

public Date getDate() {
    return date;
}

public void setDate(Date date) {
    this.date = date;
}






}
