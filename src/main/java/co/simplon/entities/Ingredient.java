package co.simplon.entities;

public class Ingredient {

    private Integer id;
    private String label;
    private int prix;
    private int quantity;


    public Integer getId() {
        return id;
    }
    public void setId(Integer id) {
        this.id = id;
    }
    public String getLabel() {
        return label;
    }
    public void setLabel(String label) {
        this.label = label;
    }
    public int getPrix() {
        return prix;
    }
    public void setPrix(int prix) {
        this.prix = prix;
    }
    public int getQuantity() {
        return quantity;
    }
    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }


    public Ingredient(Integer id, String label, int prix, int quantity) {
        this.id = id;
        this.label = label;
        this.prix = prix;
        this.quantity = quantity;
    }

    public Ingredient(String label, int prix, int quantity) {
        this.label = label;
        this.prix = prix;
        this.quantity = quantity;
    }  
    
    public Ingredient() {
    }

}
