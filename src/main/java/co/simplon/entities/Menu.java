package co.simplon.entities;

import java.util.List;

public class Menu {
    private Integer id;
    private String name;
    private int prix;
    private List<Plat> listplats;
    

    public List<Plat> getListplats() {
        return listplats;
    }
    public void setListplats(List<Plat> listplats) {
        this.listplats = listplats;
    }
    public Integer getId() {
        return id;
    }
    public void setId(Integer id) {
        this.id = id;
    }
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public int getPrix() {
        return prix;
    }
    public void setPrix(int prix) {
        this.prix = prix;
    }
    public Menu() {
    }
    public Menu(String name, int prix) {
        this.name = name;
        this.prix = prix;
    }
    public Menu(Integer id, String name, int prix) {
        this.id = id;
        this.name = name;
        this.prix = prix;
    }
    

}
