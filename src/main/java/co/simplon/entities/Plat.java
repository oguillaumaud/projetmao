package co.simplon.entities;

import java.util.ArrayList;
import java.util.List;

public class Plat {

    private Integer id;
    private String name;
    private String description;
    private List<Ingredient> ingredient = new ArrayList<>();
    private int prix;
    private String category;
    public int getPrix() {
        return prix;
    }

    public void setPrix(int prix) {
        this.prix = prix;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    private int menuId;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<Ingredient> getIngredient() {
        return ingredient;
    }

    public void setIngredient(List<Ingredient> ingredient) {
        this.ingredient = ingredient;
    }

    public Plat(String name) {
        this.name = name;
    }

    public int getMenuId() {
        return menuId;
    }

    public void setMenuId(int menuId) {
        this.menuId = menuId;
    }


    public Plat(Integer id, String name, String description, int prix, String category, int menuId) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.prix = prix;
        this.category = category;
        this.menuId = menuId;
    }

    public Plat(String name, String description, int prix, String category, int menuId) {
        this.name = name;
        this.description = description;
        this.prix = prix;
        this.category = category;
        this.menuId = menuId;
    }
    
    
    
}
