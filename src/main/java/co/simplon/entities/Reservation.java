package co.simplon.entities;

import java.util.Date;

public class Reservation {
    public Reservation(Integer id, Date date, String name) {
        this.id = id;
        this.date = date;
        this.name = name;
    }

    public Reservation() {
    }

    private Integer id;
    private Date date;
    private String name;

    
public Reservation(Date date, String name) {
        this.date = date;
        this.name = name;
    }

public Integer getId() {
    return id;
}

public void setId(Integer id) {
    this.id = id;
}


public Date getDate() {
    return date;
}

public void setDate(Date date) {
    this.date = date;
}

public String getName() {
    return name;
}

public void setName(String name) {
    this.name = name;
}







}
