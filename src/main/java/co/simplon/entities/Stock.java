package co.simplon.entities;

public class Stock {
int id;
String nom;

public Stock(int id, String nom) {
    this.id = id;
    this.nom = nom;
}


public Stock( String nom) {
    this.nom = nom;
}


public Stock() {

}


public String getNom() {
    return nom;
}


public void setNom(String nom) {
    this.nom = nom;
}


public int getId() {
    return id;
}


public void setId(int id) {
    this.id = id;
}



}
