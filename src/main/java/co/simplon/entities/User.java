package co.simplon.entities;

public class User {

    private Integer id;
    private String role;
    private String name;
  

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public User(Integer id, String role, String name) {
        this.id = id;
        this.role = role;
        this.name = name;
    }

    public User(String role, String name) {
        this.role = role;
        this.name = name;
    }

    public User(){
    }
}
