package co.simplon.repository;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import co.simplon.entities.ComandeClient;

public class CommandeClientRepository {

    public List<ComandeClient> findAll() {
        List<ComandeClient> list = new ArrayList<>();
        try (Connection connection = Database.connect()) {
            PreparedStatement stmt = connection.prepareStatement("SELECT * FROM ComandeClient");

            ResultSet result = stmt.executeQuery();

            while (result.next()) {
                ComandeClient newComandeClient = new ComandeClient(
                        result.getInt("prixTotal"),
                    result.getInt("numeroTable"));

                list.add(newComandeClient);
            }
        } catch (SQLException e) {
            System.out.println("Error: " + e.getMessage());
            e.printStackTrace();
        }
        return list;
    }

}