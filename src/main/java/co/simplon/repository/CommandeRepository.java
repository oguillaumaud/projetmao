package co.simplon.repository;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import co.simplon.entities.Commande;


public class CommandeRepository {

    public List<Commande> findAll() {
        List<Commande> list = new ArrayList<>();
        try (Connection connection = Database.connect()) {
            PreparedStatement stmt = connection.prepareStatement("SELECT * FROM commande");

            ResultSet result = stmt.executeQuery();

            while (result.next()) {
                Commande newReservation = new Commande(
                        result.getInt("prixTotal"),
                        result.getDate("date"));

                list.add(newReservation);
            }
        } catch (SQLException e) {
            System.out.println("Error: " + e.getMessage());
            e.printStackTrace();
        }
        return list;
    }

    public Commande findById(int id) {

        try (Connection connection = Database.connect()) {
            PreparedStatement stmt = connection.prepareStatement("SELECT * FROM ingredient where id =?");
            stmt.setInt(1, id);
            ResultSet result = stmt.executeQuery();

            if (result.next()) {
                return new Commande(

                        result.getInt("prixTotal"),
                        result.getDate("date"));
            }

        } catch (SQLException e) {
            System.out.println("Error: " + e.getMessage());
            e.printStackTrace();
        }

        return null;

    }

    public boolean delete(int id) {
        try (Connection connection = Database.connect()) {
            PreparedStatement stmt = connection.prepareStatement("DELETE FROM commande where id =?");
            stmt.setInt(1, id);
            int result = stmt.executeUpdate();
            if (result > 0) {
                return true;
            }

        } catch (SQLException e) {
            System.out.println("Error: " + e.getMessage());
            e.printStackTrace();
        }
        return false;
    }

    
    public boolean persist(Commande commande) {
        try (Connection connection = Database.connect()) {
            PreparedStatement stmt = connection
                    .prepareStatement("INSERT INTO commande( prixTotal, date) VALUES (?, ?)");
            stmt.setInt(1, commande.getPrixTotal());
            stmt.setDate(2, commande.getDate());
       

            if (stmt.executeUpdate() == 1) {
                 //ResultSet keys = stmt.getGeneratedKeys();
                 //keys.next();
                // Commande.setId(keys.getInt(1));
                return true;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }

    
    public boolean updateIngredient(Commande commande) {
        try (Connection connection = Database.connect()) {
            PreparedStatement stmt = connection.prepareStatement("UPDATE commande SET prixTotal =?, date = ?, WHERE id = ?");
            stmt.setInt(1, commande.getPrixTotal());
            stmt.setDate(2, commande.getDate());
           

            if (stmt.executeUpdate() == 1) {
                return true;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

    return false;   
}
}

