package co.simplon.repository;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

public class Database {
public static Connection connect() throws SQLException{
    Properties properties = new Properties();
    try {
        properties.load(Database.class.getClassLoader().getResourceAsStream("application.properties"));
    } catch (IOException e) {
        e.printStackTrace();
    }
    return DriverManager.getConnection(properties.getProperty("database.url"));
}
}


