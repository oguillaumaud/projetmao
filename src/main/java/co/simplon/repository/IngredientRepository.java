package co.simplon.repository;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import co.simplon.entities.Ingredient;

public class IngredientRepository {
    public List<Ingredient> findAll() {
        List<Ingredient> list = new ArrayList<>();
        try (Connection connection = Database.connect()) {
            PreparedStatement stmt = connection.prepareStatement("SELECT * FROM ingredient");

            ResultSet result = stmt.executeQuery();

            while (result.next()) {
                Ingredient newIngredient = new Ingredient(result.getInt("id"), result.getString("label"),  result.getInt("prix"), result.getInt("quantite"));
                list.add(newIngredient);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return list;
    }

    public Ingredient findById(int id) {

        try (Connection connection = Database.connect()) {
            PreparedStatement stmt = connection.prepareStatement("SELECT * FROM ingredient where id =?");
            stmt.setInt(1, id);
            ResultSet result = stmt.executeQuery();

            if (result.next()) {
                return new Ingredient(result.getInt("id"), result.getString("label"),  result.getInt("prix"), result.getInt("quantite"));
            }

        } catch (SQLException e) {
            System.out.println("Error: " + e.getMessage());
            e.printStackTrace();
        }

        return null;
    }

    public boolean delete(int id) {
        try (Connection connection = Database.connect()) {
            PreparedStatement stmt = connection.prepareStatement("DELETE FROM ingredient where id =?");
            stmt.setInt(1, id);
            int result = stmt.executeUpdate();
            if (result > 0) {
                return true;
            }

        } catch (SQLException e) {
            System.out.println("Error: " + e.getMessage());
            e.printStackTrace();
        }
        return false;
    }

    public boolean persist(Ingredient ingredient) {
        try (Connection connection = Database.connect()) {
            PreparedStatement stmt = connection
                    .prepareStatement("INSERT INTO ingredient(label, prix, quantite) VALUES (?, ?, ?)");
            stmt.setString(1, ingredient.getLabel());
            stmt.setInt(2, ingredient.getPrix());
            stmt.setInt(3, ingredient.getQuantity());

            if (stmt.executeUpdate() == 1) {
                // ResultSet keys = stmt.getGeneratedKeys();
                // keys.next();
                // ingredient.setId(keys.getInt(1));
                return true;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }

    public boolean updateIngredient(Ingredient ingredient) {
            try (Connection connection = Database.connect()) {
                PreparedStatement stmt = connection.prepareStatement("UPDATE ingredient SET label = ?, prix = ?, quantite = ? WHERE id = ?");
                stmt.setString(1, ingredient.getLabel());
                stmt.setInt(2, ingredient.getPrix());
                stmt.setInt(3, ingredient.getQuantity());
                stmt.setInt(4, ingredient.getId());

                if (stmt.executeUpdate() == 1) {
                    return true;
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }

        return false;   
    }
}