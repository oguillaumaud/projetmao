package co.simplon.repository;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import co.simplon.entities.Menu;

public class MenuRepository {

    public List<Menu> findAll() {
        List<Menu> list = new ArrayList<>();
        try (Connection connection = Database.connect()) {
            PreparedStatement stmt = connection.prepareStatement("SELECT * FROM menu");

            ResultSet result = stmt.executeQuery();

            while (result.next()) {
                Menu newMenu = new Menu(result.getString("name"), result.getInt("prix"));
                list.add(newMenu);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return list;
    }

    public Menu findById(int id) {

        try (Connection connection = Database.connect()) {
            PreparedStatement stmt = connection.prepareStatement("SELECT * FROM menu where id =?");
            stmt.setInt(1, id);
            ResultSet result = stmt.executeQuery();

            if (result.next()) {
                return new Menu(result.getString("name"), result.getInt("prix"));
            }

        } catch (SQLException e) {
            System.out.println("Error: " + e.getMessage());
            e.printStackTrace();
        }

        return null;
    }

    public boolean delete(int id) {
        try (Connection connection = Database.connect()) {
            PreparedStatement stmt = connection.prepareStatement("DELETE FROM menu where id =?");
            stmt.setInt(1, id);
            int result = stmt.executeUpdate();
            if (result > 0) {
                return true;
            }

        } catch (SQLException e) {
            System.out.println("Error: " + e.getMessage());
            e.printStackTrace();
        }
        return false;
    }

    public boolean persist(Menu Menu) {
        try (Connection connection = Database.connect()) {
            PreparedStatement stmt = connection
                    .prepareStatement("INSERT INTO menu(name, prix) VALUES (?, ?)");
            stmt.setString(1, Menu.getName());
            stmt.setInt(2, Menu.getPrix());

            if (stmt.executeUpdate() == 1) {
                // ResultSet keys = stmt.getGeneratedKeys();
                // keys.next();
                // Menu.setId(keys.getInt(1));
                return true;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }

    public boolean updateMenu(Menu Menu) {
            try (Connection connection = Database.connect()) {
                PreparedStatement stmt = connection.prepareStatement("UPDATE Menu SET name = ?, prix = ? WHERE id = ?");
                stmt.setString(1, Menu.getName());
                stmt.setInt(2, Menu.getPrix());
                stmt.setInt(4, Menu.getId());

                if (stmt.executeUpdate() == 1) {
                    return true;
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }

        return false;   
    }
}
