package co.simplon.repository;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import co.simplon.entities.Ingredient;
import co.simplon.entities.Plat;

public class PlatRepository {

    public List<Plat> findAll() {
        List<Plat> list = new ArrayList<>();
        try (Connection connection = Database.connect()) {
            PreparedStatement stmt = connection.prepareStatement("SELECT*FROM plat");
            ResultSet result = stmt.executeQuery();
            while (result.next()) {
                Plat plat = new Plat(result.getInt("id"), result.getString("nom"), result.getString("description"), result.getInt("prix"),result.getString("category"), result.getInt("menu_id"));
                list.add(plat);
            }
        } catch (SQLException e) {
            System.out.println("Error From repository");
            e.printStackTrace();
        }
        return list;
    }

    public Plat findById(int id) {
        try (Connection connection = Database.connect()) {
            PreparedStatement stmt = connection.prepareStatement("SELECT*FROM plat WHERE id = ?");
            stmt.setInt(1, id);
            ResultSet result = stmt.executeQuery();
            if (result.next()) {
                return new Plat(result.getInt("id"), result.getString("nom"), result.getString("description"), result.getInt("prix"),result.getString("category"), result.getInt("menu_id"));
            }
        } catch (SQLException e) {
            System.out.println("Error From repository");
            e.printStackTrace();
        }
        return null;
    }

    public List<Ingredient> listIngredients(int id) {
        List<Ingredient> list = new ArrayList<>();
        try (Connection connection = Database.connect()) {
            PreparedStatement stmt = connection.prepareStatement("SELECT*FROM ingredient LEFT JOIN ingredient_plat ON ingredient_plat.ingredient_id = ingredient.id LEFT JOIN plat ON ingredient_plat.plat_id = plat.id WHERE plat.id = ?");
            stmt.setInt(1, id);
            ResultSet result = stmt.executeQuery();
            while (result.next()) {
                Ingredient ingredient = new Ingredient(result.getInt("id"), result.getString("label"), result.getInt("prix"), result.getInt("quantite"));
                list.add(ingredient);
            }
        } catch (SQLException e) {
            System.out.println("Error From repository");
            e.printStackTrace();
        }
        return list;
    }

    public Boolean deleteByID(int id) {
        try (Connection connection = Database.connect()) {
            PreparedStatement stmt = connection.prepareStatement("DELETE FROM plat WHERE id = ?");
            stmt.setInt(1, id);
            int result = stmt.executeUpdate();
            if (result == 1) {
                return true;
            }
        } catch (SQLException e) {
            System.out.println("Error From repository");
            e.printStackTrace();
        }
        return false;
    }

    public List<Plat> findByCategory(String category) {
        List<Plat> list = new ArrayList<>();
        try (Connection connection = Database.connect()) {
            PreparedStatement stmt = connection.prepareStatement("SELECT*FROM plat WHERE category = ?");
            stmt.setString(1, category);
            ResultSet result = stmt.executeQuery();
            while (result.next()) {
                Plat plat = new Plat(result.getInt("id"), result.getString("nom"), result.getString("description"), result.getInt("prix"),result.getString("category"), result.getInt("menu_id"));
                list.add(plat);
            }
        } catch (SQLException e) {
            System.out.println("Error From repository");
            e.printStackTrace();
        }
        return list;
    }

    public Boolean persist(Plat plat) {
        try (Connection connection = Database.connect()) {
            PreparedStatement stmt = connection.prepareStatement(
                    "INSERT INTO plat(nom,description,prix,category,menu_id)Values (?,?,?,?,?)", Statement.RETURN_GENERATED_KEYS);
            stmt.setString(1, plat.getName());
            stmt.setString(2, plat.getDescription());
            stmt.setInt(3, plat.getPrix());
            stmt.setString(4, plat.getCategory());
            stmt.setInt(5, plat.getMenuId());
            int result = stmt.executeUpdate();
            if (result == 1) {
                ResultSet rs = stmt.getGeneratedKeys();
                rs.next();
                plat.setId(rs.getInt(1));
                return true;
            }
        } catch (SQLException e) {
            System.out.println("Error From repository");
            e.printStackTrace();
        }
        return false;
    }

    public Boolean updatePlat(Plat plat){
        try (Connection connection = Database.connect()) {
            PreparedStatement stmt = connection.prepareStatement("UPDATE plat SET nom=?, description=?, prix=?, category=?, menu_id=? WHERE id = ?");
            if (plat != null){
            stmt.setString(1, plat.getName());
            stmt.setString(2, plat.getDescription());
            stmt.setInt(3, plat.getPrix());
            stmt.setString(4, plat.getCategory());
            stmt.setInt(5, plat.getMenuId());
            stmt.setInt(6, plat.getId());
            return stmt.executeUpdate() == 1;   
        }
        } catch (SQLException e) {
            System.out.println("Error From repository");
            e.printStackTrace();
        }
        return false;
    }
}
