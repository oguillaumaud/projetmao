package co.simplon.repository;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import co.simplon.entities.Reservation;

public class ReservationRepository {

    public List<Reservation> findAll() {
        List<Reservation> list = new ArrayList<>();
        try (Connection connection = Database.connect()) {
            PreparedStatement stmt = connection.prepareStatement("SELECT * FROM reservation");

            ResultSet result = stmt.executeQuery();

            while (result.next()) {
                Reservation newReservation = new Reservation(result.getInt("id"),  result.getDate("date"), result.getString("name"));
                list.add(newReservation);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return list;
    }

    public Reservation findById(int id) {

        try (Connection connection = Database.connect()) {
            PreparedStatement stmt = connection.prepareStatement("SELECT * FROM reservation where id =?");
            stmt.setInt(1, id);
            ResultSet result = stmt.executeQuery();

            if (result.next()) {
                return new Reservation(result.getInt("id"),  result.getDate("date"), result.getString("name"));
            }

        } catch (SQLException e) {
            System.out.println("Error: " + e.getMessage());
            e.printStackTrace();
        }

        return null;
    }

    public boolean delete(int id) {
        try (Connection connection = Database.connect()) {
            PreparedStatement stmt = connection.prepareStatement("DELETE FROM reservation where id =?");
            stmt.setInt(1, id);
            int result = stmt.executeUpdate();
            if (result > 0) {
                return true;
            }

        } catch (SQLException e) {
            System.out.println("Error: " + e.getMessage());
            e.printStackTrace();
        }
        return false;
    }

    public boolean persist(Reservation Reservation) {
        try (Connection connection = Database.connect()) {
            PreparedStatement stmt = connection
                    .prepareStatement("INSERT INTO reservation (name, date) VALUES (?, ?)");
                    stmt.setString(1, Reservation.getName());
                    stmt.setDate(2,  new java.sql.Date(Reservation.getDate().getTime()));

            if (stmt.executeUpdate() == 1) {
                // ResultSet keys = stmt.getGeneratedKeys();
                // keys.next();
                // Reservation.setId(keys.getInt(1));
                return true;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }

    public boolean updateReservation(Reservation Reservation) {
            try (Connection connection = Database.connect()) {
                PreparedStatement stmt = connection.prepareStatement("UPDATE Reservation SET name = ?, date WHERE id = ?");
                stmt.setString(1, Reservation.getName());
                stmt.setDate(2, (Date) Reservation.getDate());
                stmt.setInt(3, Reservation.getId());
                if (stmt.executeUpdate() == 1) {
                    return true;
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }

        return false;   
    }
}
