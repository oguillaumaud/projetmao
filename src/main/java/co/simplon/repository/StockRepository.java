package co.simplon.repository;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import co.simplon.entities.Ingredient;
import co.simplon.entities.Stock;

public class StockRepository {

    public List<Stock> findAll() {
        List<Stock> list = new ArrayList<>();
        try (Connection connection = Database.connect()) {
            PreparedStatement stmt = connection.prepareStatement("SELECT*FROM stock");
            ResultSet result = stmt.executeQuery();
            while (result.next()) {
                Stock stock = new Stock(result.getInt("id"), result.getString("name"));
                list.add(stock);
            }
        } catch (SQLException e) {
            System.out.println("Error From repository");
            e.printStackTrace();
        }
        return list;
    }

    public Stock findById(int id) {
        try (Connection connection = Database.connect()) {
            PreparedStatement stmt = connection.prepareStatement("SELECT*FROM stock WHERE id = ?");
            stmt.setInt(1, id);
            ResultSet result = stmt.executeQuery();
            if (result.next()) {
                return new Stock(result.getInt("id"), result.getString("name"));
            }
        } catch (SQLException e) {
            System.out.println("Error From repository");
            e.printStackTrace();
        }
        return null;
    }

    public Boolean deleteByID(int id) {
        try (Connection connection = Database.connect()) {
            PreparedStatement stmt = connection.prepareStatement("DELETE FROM stock WHERE id = ?");
            stmt.setInt(1, id);
            int result = stmt.executeUpdate();
            if (result == 1) {
                return true;
            }
        } catch (SQLException e) {
            System.out.println("Error From repository");
            e.printStackTrace();
        }
        return false;
    }

    public Boolean persist(Stock stock) {
        try (Connection connection = Database.connect()) {
            PreparedStatement stmt = connection.prepareStatement(
                    "INSERT INTO stock(name)Values (?)", Statement.RETURN_GENERATED_KEYS);
            stmt.setString(1, stock.getNom());
            int result = stmt.executeUpdate();
            if (result == 1) {
                ResultSet rs = stmt.getGeneratedKeys();
                rs.next();
                stock.setId(rs.getInt(1));
                return true;
            }
        } catch (SQLException e) {
            System.out.println("Error From repository");
            e.printStackTrace();
        }
        return false;
    }

    public Boolean updateStock(Stock stock){
        try (Connection connection = Database.connect()) {
            PreparedStatement stmt = connection.prepareStatement("UPDATE stock SET name = ? WHERE id = ?");
            if (stock != null){
            stmt.setString(1, stock.getNom());
            stmt.setInt(2, stock.getId());
            return stmt.executeUpdate() == 1;  
            } 
        } catch (SQLException e) {
            System.out.println("Error From repository");
            e.printStackTrace();
        }
        return false;
    }

    public List<Ingredient> stockList(int id) {
        List<Ingredient> list = new ArrayList<>();
        try (Connection connection = Database.connect()) {
            PreparedStatement stmt = connection.prepareStatement("SELECT*FROM ingredient WHERE stock_id = ?");
            stmt.setInt(1, id);
            ResultSet result = stmt.executeQuery();
            while (result.next()) {
                Ingredient ingredient = new Ingredient(result.getInt("id"), result.getString("label"), result.getInt("prix"), result.getInt("quantite"));
                list.add(ingredient);
            }
        } catch (SQLException e) {
            System.out.println("Error From repository");
            e.printStackTrace();
        }
        return list;
    }
}
