package co.simplon.repository;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import co.simplon.entities.User;

public class UserRepository {
    
    public List<User> findAll() {
        List<User> list = new ArrayList<>();
        try (Connection connection = Database.connect()) {
            PreparedStatement stmt = connection.prepareStatement("SELECT * FROM users");
            ResultSet result = stmt.executeQuery();
            while (result.next()) {
                User user = new User(result.getInt("id"), result.getString("role"), result.getString("name"));
                list.add(user);
            }
        } catch (SQLException e) {
            System.out.println("Error From repository");
            e.printStackTrace();
        }
        return list;
    }

    public User findById(int id) {
        try (Connection connection = Database.connect()) {
            PreparedStatement stmt = connection.prepareStatement("SELECT * FROM users WHERE id = ?");
            stmt.setInt(1, id);
            ResultSet result = stmt.executeQuery();
            if (result.next()) {
                return new User(result.getInt("id"), result.getString("role"), result.getString("name"));
            }
        } catch (SQLException e) {
            System.out.println("Error From repository");
            e.printStackTrace();
        }
        return null;
    }

    public Boolean deleteByID(int id) {
        try (Connection connection = Database.connect()) {
            PreparedStatement stmt = connection.prepareStatement("DELETE FROM users WHERE id = ?");
            stmt.setInt(1, id);
            int result = stmt.executeUpdate();
            if (result == 1) {
                return true;
            }
        } catch (SQLException e) {
            System.out.println("Error From repository");
            e.printStackTrace();
        }
        return false;
    }

    /*
     * public List<User> findByCategory(String category) {
     * List<User> list = new ArrayList<>();
     * try (Connection connection = Database.connect()) {
     * PreparedStatement stmt =
     * connection.prepareStatement("SELECT*FROM users WHERE category = ?");
     * stmt.setString(1, category);
     * ResultSet result = stmt.executeQuery();
     * while (result.next()) {
     * User user = new User(result.getInt("id"), result.getString("role"),
     * result.getString("name"));
     * list.add(user);
     * }
     * } catch (SQLException e) {
     * System.out.println("Error From repository");
     * e.printStackTrace();
     * }
     * return list;
     * }
     */

    public boolean updateUser(User user) {
        try (Connection connection = Database.connect()) {
            PreparedStatement stmt = connection.prepareStatement("UPDATE users SET name = ?, role = ? WHERE id = ?");
            if (user == null){
            stmt.setString(1, user.getName());
            stmt.setString(2, user.getRole());
            stmt.setInt(3, user.getId());
            return stmt.executeUpdate() == 1;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return false;
    }

    public Boolean persist(User user) {
        try (Connection connection = Database.connect()) {
            PreparedStatement stmt = connection.prepareStatement(
                    "INSERT INTO users (name,role)Values (?,?)", Statement.RETURN_GENERATED_KEYS);
            //stmt.setInt(1, user.getId());
            stmt.setString(1, user.getName());
            stmt.setString(2, user.getRole());

            int result = stmt.executeUpdate();
            if (result == 1) {
                ResultSet rs = stmt.getGeneratedKeys();
                rs.next();
                user.setId(rs.getInt(1));
                return true;
            }
        } catch (SQLException e) {
            System.out.println("Error From repository");
            e.printStackTrace();
        }
        return false;
    }

}