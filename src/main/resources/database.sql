-- Active: 1709546324780@@127.0.0.1@3306@projetmao

>>>>>>> 21c0c14973ff65af4df8287c5a8be73c2658696d
DROP TABLE IF EXISTS commandeClient;
DROP TABLE IF EXISTS reservation;
DROP TABLE IF EXISTS users;
DROP TABLE IF EXISTS ingredient_plat;
DROP TABLE IF EXISTS plat;
DROP TABLE IF EXISTS menu;
DROP TABLE IF EXISTS ingredient;
DROP TABLE IF EXISTS stock;
DROP TABLE IF EXISTS menu;


CREATE TABLE reservation (
  id INT PRIMARY KEY AUTO_INCREMENT,
  name VARCHAR(255) NOT NULL,
  date DATE NOT NULL
);


CREATE TABLE users (
  id INT PRIMARY KEY AUTO_INCREMENT,
  role VARCHAR(255) NOT NULL,
  name VARCHAR(255) NOT NULL
);

CREATE TABLE commande(
  id INT PRIMARY KEY AUTO_INCREMENT,
  prixTotals INT NOT NULL,
  date DATE
);

CREATE TABLE stock(
  id INT PRIMARY KEY AUTO_INCREMENT,
  name VARCHAR(255) NOT NULL
);

CREATE TABLE ingredient (
  id INT PRIMARY KEY AUTO_INCREMENT,
  label VARCHAR(255) NOT NULL,
  prix INT NOT NULL,
  quantite INT,
  commande_id INT,
  FOREIGN KEY (commande_id) REFERENCES commande(id) ON DELETE SET NULL,
  stock_id INT,
  FOREIGN KEY (stock_id) REFERENCES stock(id) ON DELETE SET NULL
);

CREATE TABLE menu(
  id INT PRIMARY KEY AUTO_INCREMENT,
  name VARCHAR(255) NOT NULL,
  prix INT NOT NULL
);

CREATE TABLE plat(
  id INT PRIMARY KEY AUTO_INCREMENT,
  nom VARCHAR(255) NOT NULL,
  description VARCHAR(255) NOT NULL,
  prix INT NOT NULL,
  category VARCHAR(255) NOT NULL,
    menu_id INT NOT NULL,
  FOREIGN KEY (menu_id) REFERENCES menu(id)
);

CREATE TABLE ingredient_plat(
  ingredient_id INT,
  plat_id INT,
  PRIMARY KEY (ingredient_id, plat_id),
  FOREIGN KEY (plat_id) REFERENCES plat(id) ON DELETE CASCADE,
  FOREIGN KEY (ingredient_id) REFERENCES ingredient(id)
);

CREATE TABLE commandeClient (
  id INT PRIMARY key AUTO_INCREMENT,
prixTotal INT,
numéroTable INT,
listplat VARCHAR(255) NOT NULL,
listmenu VARCHAR(255) NOT NULL
); 


INSERT INTO reservation (name, date) VALUES
('Réservation 1', '2024-03-15'),
('Réservation 2', '2024-03-16'),
('Réservation 3', '2024-03-17'),
('Réservation 4', '2024-03-18'),
('Réservation 5', '2024-03-19'),
('Réservation 6', '2024-03-20'),
('Réservation 7', '2024-03-21'),
('Réservation 8', '2024-03-22'),
('Réservation 9', '2024-03-23'),
('Réservation 10', '2024-03-24'),
('Réservation 11', '2024-03-25'),
('Réservation 12', '2024-03-26'),
('Réservation 13', '2024-03-27'),
('Réservation 14', '2024-03-28'),
('Réservation 15', '2024-03-29');

-- Insertions dans la table "users"
INSERT INTO users (role, name) VALUES
('Administrateur', 'Jean Dupont'),
('Serveur', 'Marie Leclerc'),
('Client', 'Pierre Martin'),
('Chef de cuisine', 'Sophie Lefebvre'),
('Assistant de service', 'Thomas Dubois'),
('Manager', 'Isabelle Renault'),
('Cuisinier', 'Luc Martin'),
('Serveuse', 'Amélie Girard'),
('Assistant de cuisine', 'Nicolas Lambert'),
('Hôte', 'Émilie Bernard'),
('Responsable des achats', 'Philippe Moreau'),
('Caissier', 'Julie Lavoie'),
('Directeur', 'Antoine Gagnon'),
('Sommelier', 'Charlotte Lambert'),
('Barman', 'Alexandre Roy');

-- Insertions dans la table "commande"
INSERT INTO commande (prixTotals, date) VALUES
(50, '2024-03-15'),
(70, '2024-03-16'),
(90, '2024-03-17'),
(120, '2024-03-18'),
(80, '2024-03-19'),
(110, '2024-03-20'),
(65, '2024-03-21'),
(95, '2024-03-22'),
(75, '2024-03-23'),
(100, '2024-03-24'),
(85, '2024-03-25'),
(115, '2024-03-26'),
(130, '2024-03-27'),
(55, '2024-03-28'),
(105, '2024-03-29');

-- Insertions dans la table "stock"
INSERT INTO stock (name) VALUES
('Stock principal'),
('Stock de secours'),
('Stock de saison');

-- Insertions dans la table "ingredient"
INSERT INTO ingredient (label, prix, quantite, commande_id, stock_id) VALUES
("OSEF 1", 5, 100, 1, 1),
("OSEF", 3, 50, 2, 1),
("OSEF", 4, 80, 3, 1),
("OSEF", 6, 120, 4, 1),
("OSEF", 2, 70, 5, 2),
("OSEF", 8, 90, 6, 2),
("OSEF", 7, 65, 7, 2),
("OSEF", 9, 95, 8, 2),
("OSEF", 10, 75, 9, 2),
("OSEF", 12, 100, 10, 3),
("OSEF", 11, 85, 11, 3),
("OSEF", 14, 115, 12, 3),
("OSEF", 13, 130, 13, 3),
("OSEF", 15, 55, 14, 3),
("OSEF", 16, 105, 15, 1);

-- Insertions dans la table "menu"
INSERT INTO menu (name, prix) VALUES
('Menu 1', 25),
('Menu 2', 30),
('Menu 3', 35),
('Menu 4', 40),
('Menu 5', 45),
('Menu 6', 50),
('Menu 7', 55),
('Menu 8', 60),
('Menu 9', 65),
('Menu 10', 70),
('Menu 11', 75),
('Menu 12', 80),
('Menu 13', 85),
('Menu 14', 90),
('Menu 15', 95);

-- Insertions dans la table "plat"
INSERT INTO plat (nom, description, prix, category, menu_id) VALUES
('Entrée 1', 'Description de l\'entrée 1', 15, 'Entrée', 1),
('Plat principal 1', 'Description du plat principal 1', 20, 'Plat principal', 2),
('Dessert 1', 'Description du dessert 1', 10, 'Dessert', 3),
('Entrée 2', 'Description de l\'entrée 2', 18, 'Entrée', 4),
('Plat principal 2', 'Description du plat principal 2', 22, 'Plat principal', 5),
('Dessert 2', 'Description du dessert 2', 12, 'Dessert', 6),
('Entrée 3', 'Description de l\'entrée 3', 17, 'Entrée', 7),
('Plat principal 3', 'Description du plat principal 3', 21, 'Plat principal', 8),
('Dessert 3', 'Description du dessert 3', 11, 'Dessert', 9),
('Entrée 4', 'Description de l\'entrée 4', 16, 'Entrée', 10),
('Plat principal 4', 'Description du plat principal 4', 23, 'Plat principal', 11),
('Dessert 4', 'Description du dessert 4', 13, 'Dessert', 12),
('Entrée 5', 'Description de l\'entrée 5', 19, 'Entrée', 13),
('Plat principal 5', 'Description du plat principal 5', 24, 'Plat principal', 14),
('Dessert 5', 'Description du dessert 5', 14, 'Dessert', 15);

-- Insertions dans la table "ingredient_plat"
INSERT INTO ingredient_plat (ingredient_id, plat_id) VALUES
(1, 1),
(2, 2),
(3, 3),
(4, 4),
(5, 5),
(6, 6),
(7, 7),
(8, 8),
(9, 9),
(10, 10),
(11, 11),
(12, 12),
(13, 13),
(14, 14),
(15, 15);