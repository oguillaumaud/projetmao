

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.fail;

import java.io.InputStreamReader;
import java.sql.SQLException;
import java.util.List;

import org.apache.ibatis.jdbc.ScriptRunner;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import co.simplon.entities.Ingredient;
import co.simplon.repository.Database;
import co.simplon.repository.IngredientRepository;

public class IngredientRepositoryTest {
    @BeforeEach
    void setUp() {
        try {
            ScriptRunner runner = new ScriptRunner(Database.connect());
            runner.runScript(new InputStreamReader(getClass().getResourceAsStream("/database.sql")));
        } catch (SQLException e) {
            e.printStackTrace();
            fail();
            System.out.println("erreur foreach");
        }
    }

    @Test
    public void testFindAll() {
                IngredientRepository repo = new IngredientRepository();
                List<Ingredient> result = repo.findAll();
                assertEquals(15, result.size() ) ;
    }

    @Test
    public void testFindById() {
                IngredientRepository repo = new IngredientRepository();
                Ingredient result = repo.findById(1);
                assertEquals("OSEF 1", result.getLabel() ) ;
    }

    @Test
    public void errorTestFindById() {
                IngredientRepository repo = new IngredientRepository();
                Ingredient result = repo.findById(100);
                assertNull(result ) ;
    }
}
