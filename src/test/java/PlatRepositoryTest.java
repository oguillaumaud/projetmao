import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;

import java.io.InputStreamReader;
import java.sql.SQLException;
import java.util.List;

import org.apache.ibatis.jdbc.ScriptRunner;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import co.simplon.entities.Ingredient;
import co.simplon.entities.Plat;
import co.simplon.repository.Database;
import co.simplon.repository.PlatRepository;

public class PlatRepositoryTest {

        @BeforeEach
    void setUp() {
        try {
            ScriptRunner runner = new ScriptRunner(Database.connect());
            runner.runScript(new InputStreamReader(getClass().getResourceAsStream("/database.sql")));
        } catch (SQLException e) {
            e.printStackTrace();
            fail();
            System.out.println("erreur foreach");
        }
    }

    @Test
    void findAllShouldReturnAListOfPlat (){
        PlatRepository repo = new PlatRepository();
        List<Plat> result = repo.findAll();
        assertEquals(15, result.size());
        assertEquals(1, result.get(0).getId());
        assertEquals("Entrée 1", result.get(0).getName());
        assertEquals("Description de l'entrée 1", result.get(0).getDescription());
        assertEquals(15, result.get(0).getPrix());
        assertEquals("Entrée", result.get(0).getCategory());
        assertEquals(1, result.get(0).getMenuId());
    }

    @Test
    void findByIdWithResult (){
        PlatRepository repo = new PlatRepository();
        Plat testPlat = repo.findById(1);
        assertEquals(1, testPlat.getId());
        assertEquals("Entrée 1", testPlat.getName());
        assertEquals("Description de l'entrée 1", testPlat.getDescription());
        assertEquals(15, testPlat.getPrix());
        assertEquals("Entrée", testPlat.getCategory());
        assertEquals(1, testPlat.getMenuId());
    }

    @Test
    void findByIdNoResult (){
        PlatRepository repo = new PlatRepository();
        Plat testPlat = repo.findById(1000);
        assertNull(testPlat);
    }

    @Test
    void deleteSuccess(){
        PlatRepository repo = new PlatRepository();
        Boolean result = repo.deleteByID(2);
        assertTrue(result);
    }

    @Test
    void persistSuccess(){
        PlatRepository repo = new PlatRepository();
        Plat testPlat = new Plat("testname", "test", 4, "testCat", 1);
        Boolean result = repo.persist(testPlat);
        assertTrue(result);
    }

    @Test
    void persistNotSuccess(){
        PlatRepository repo = new PlatRepository();
        Plat testPlat = new Plat(null);
        Boolean result = repo.persist(testPlat);
        assertFalse(result);
    }

    @Test
    void updateSuccess(){
        PlatRepository repo = new PlatRepository();
        Plat testPlat = new Plat(1,"testname", "test", 4, "testCat", 1);
        Boolean result = repo.updatePlat(testPlat);
        assertTrue(result);
    }
    
    @Test
    void updateNotSuccess(){
        PlatRepository repo = new PlatRepository();
        Boolean result = repo.updatePlat(null);
        assertFalse(result);
    }

    @Test
    void findListIngredient(){
        PlatRepository repo = new PlatRepository();
        List<Ingredient> result = repo.listIngredients(1);
        assertEquals(1, result.size());
        assertEquals("OSEF 1", result.get(0).getLabel());
    }

    @Test
    void findByCategoryWithResult (){
        PlatRepository repo = new PlatRepository();
        List<Plat> result= repo.findByCategory("Entrée");
        assertEquals(5, result.size());
        assertEquals("Entrée", result.get(0).getCategory());
    }

}
