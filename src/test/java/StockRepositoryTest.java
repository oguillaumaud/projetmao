import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;

import java.io.InputStreamReader;
import java.sql.SQLException;
import java.util.List;

import org.apache.ibatis.jdbc.ScriptRunner;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import co.simplon.entities.Ingredient;
import co.simplon.entities.Stock;
import co.simplon.repository.Database;
import co.simplon.repository.StockRepository;

public class StockRepositoryTest {

    @BeforeEach
    void setUp() {
        try {
            ScriptRunner runner = new ScriptRunner(Database.connect());
            runner.runScript(new InputStreamReader(getClass().getResourceAsStream("/database.sql")));
        } catch (SQLException e) {
            e.printStackTrace();
            fail();
            System.out.println("erreur foreach");
        }
    }

    @Test
    void findAllShouldReturnAListOfStock (){
        StockRepository repo = new StockRepository();
        List<Stock> result = repo.findAll();
        assertEquals(3, result.size());
        assertEquals("Stock principal", result.get(0).getNom());
    }

    @Test
    void findByIdWithResult (){
        StockRepository repo = new StockRepository();
        Stock testStock = repo.findById(1);
        assertEquals(1, testStock.getId());
        assertEquals("Stock principal", testStock.getNom());
    }

    @Test
    void findByIdNoResult (){
        StockRepository repo = new StockRepository();
        Stock testStock = repo.findById(1000);
        assertNull(testStock);
    }

    @Test
    void deleteSuccess(){
        StockRepository repo = new StockRepository();
        Boolean result = repo.deleteByID(2);
        assertTrue(result);
    }

    @Test
    void persistSuccess(){
        StockRepository repo = new StockRepository();
        Stock stockTest = new Stock("test");
        Boolean result = repo.persist(stockTest);
        assertTrue(result);
    }

    @Test
    void persistNotSuccess(){
        StockRepository repo = new StockRepository();
        Stock stockTest = new Stock();
        Boolean result = repo.persist(stockTest);
        assertFalse(result);
    }

    @Test
    void updateSuccess(){
        StockRepository repo = new StockRepository();
        Boolean result = repo.updateStock(new Stock(1, "test"));
        assertTrue(result);
    }
    
    @Test
    void updateNotSuccess(){
        StockRepository repo = new StockRepository();
        Boolean result = repo.updateStock(null);
        assertFalse(result);
    }

    @Test
    void findListStock(){
        StockRepository repo = new StockRepository();
        List<Ingredient> result = repo.stockList(1);
        assertEquals(5, result.size());
        assertEquals("OSEF 1", result.get(0).getLabel());
    }
}
