import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;

import java.io.InputStreamReader;
import java.sql.SQLException;
import java.util.List;

import org.apache.ibatis.jdbc.ScriptRunner;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import co.simplon.entities.User;
import co.simplon.repository.Database;

import co.simplon.repository.UserRepository;

public class UserRepositoryTest {
    @BeforeEach
    void setUp() {
        try {
            ScriptRunner runner = new ScriptRunner(Database.connect());
            runner.runScript(new InputStreamReader(getClass().getResourceAsStream("/database.sql")));
        } catch (SQLException e) {
            e.printStackTrace();
            fail();
            System.out.println("erreur recherche");
        }
    }

    @Test
    void testFindAll() {
        UserRepository repo = new UserRepository();
        List<User> result = repo.findAll();
        assertEquals(15, result.size());
    }

    @Test
    void testFindById() {
        UserRepository repo = new UserRepository();
        User result = repo.findById(1);
        assertEquals("Administrateur", result.getRole());
    }

    @Test
    void deleteReussi() {
        UserRepository repo = new UserRepository();
        Boolean result = repo.deleteByID(3);
        assertTrue(result);
    }

    @Test
    void updateReussi() {
        UserRepository repo = new UserRepository();
        Boolean result = repo.updateUser(new User(1, "apprentie", "Doume"));
        assertTrue(result);
    }

    @Test
    void updatePasReussi() {
        UserRepository repo = new UserRepository();
        Boolean result = repo.updateUser(new User());
        assertFalse(result);
    }

    @Test
    void persistSuccess() {
        UserRepository repo = new UserRepository();
        User userTest = new User("test", "TESTkd");
        Boolean result = repo.persist(userTest);
        assertTrue(result);
    }

    @Test
    void persistPasSuccess() {
        UserRepository repo = new UserRepository();
        User userTest = new User();
        Boolean result = repo.persist(userTest);
        assertFalse(result);
    }
}


